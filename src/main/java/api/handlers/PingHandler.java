package api.handlers;

import api.service.PingService;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Singleton;

@Singleton
public class PingHandler {

    private final PingService pingService;

    public PingHandler(PingService pingService) {
        this.pingService = pingService;
    }

    public void getHelloWorld(RoutingContext routingContext) {
        routingContext.response().setChunked(true);
        String pingResponse = pingService.pingTCP();
        routingContext.response().write(pingResponse);
        routingContext.response().end();
    }

    public void getHelloWorld2(RoutingContext routingContext) {
        routingContext.response().setChunked(true);
        String pingResponse = pingService.pingTCP();
        routingContext.response().write(pingResponse);
        routingContext.response().end();
    }

}
