package api.verticles;

import api.util.ServiceBuilder;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;

import javax.inject.Singleton;

@Singleton
public class AppVerticle extends AbstractVerticle implements NamedVerticle {

    private final int targetPort = 8080;
    private HttpServer httpServer;
    private final ServiceBuilder serviceBuilder;

    public AppVerticle(ServiceBuilder serviceBuilder) {
        this.serviceBuilder = serviceBuilder;
    }

    @Override
    public String name() {
        return "Ping Service";
    }

    @Override
    public void start(Promise<Void> verticleStart) {
        serviceBuilder.buildAPIServer(targetPort)
                .onSuccess(httpServer -> {
                    this.httpServer = httpServer;
                    verticleStart.complete();
                })
                .onFailure(verticleStart::fail);
    }

    @Override
    public void stop(Promise<Void> serverShutDown) {
        httpServer.close(close -> {
            if (close.failed()) {
                serverShutDown.fail(close.cause());
            } else {
                serverShutDown.complete();
            }
        });
    }
}
