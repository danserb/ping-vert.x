package api.verticles;

import io.vertx.core.Verticle;

public interface NamedVerticle extends Verticle {
    String name();
}
