package api.toolkit;

import io.vertx.core.Vertx;

import javax.inject.Singleton;

@Singleton
public class AppToolKit {

    private final Vertx vertx = buildVertxEngine();

    private Vertx buildVertxEngine() {
        return Vertx.vertx();
    }

    public Vertx getVertx() {
        return vertx;
    }
}
