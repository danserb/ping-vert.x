package api.deployment;

import api.toolkit.AppToolKit;
import api.util.DeploymentHelper;
import api.verticles.AppVerticle;
import io.vertx.core.Future;

import javax.inject.Singleton;

@Singleton
public class DeployService {

    private final AppToolKit appToolKit;
    private final AppVerticle appVerticle;

    public DeployService(AppToolKit appToolKit, AppVerticle appVerticle) {
        this.appToolKit = appToolKit;
        this.appVerticle = appVerticle;
    }

    public Future<?> deployServiceAPI() {
        try {
            return DeploymentHelper.deployVerticle(appToolKit.getVertx(), appVerticle);
        } catch (Exception e) {
            return Future.failedFuture(e);
        }
    }
}
