package api.service;

import javax.inject.Singleton;

@Singleton
public class PingServiceImpl implements PingService {

    @Override
    public String pingTCP() {
        return "Ping TCP Successful";
    }
}
