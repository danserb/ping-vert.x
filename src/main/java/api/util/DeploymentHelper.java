package api.util;

import api.verticles.NamedVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

public class DeploymentHelper {
    public static Future<Void> deployVerticle(Vertx vertx, NamedVerticle verticle) {
        return deployVerticle(vertx, verticle, verticle.name());
    }

    private static Future<Void> deployVerticle(Vertx vertx, NamedVerticle verticle, String verticleName) {
        Promise<Void> promise = Promise.promise();
        vertx.deployVerticle(verticle, res -> {
            if (res.failed())  {
                System.out.println(String.format("Failed to deploy verticle %s", verticleName));
                promise.fail(res.cause());
            } else {
                System.out.println(String.format("Deployed %s verticle", verticleName));
                promise.complete();
            }
        });
        return promise.future();
    }
}
