package api.util;

import api.handlers.PingHandler;
import api.toolkit.AppToolKit;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;

import javax.inject.Singleton;

@Singleton
public class ServiceBuilder {

    private final Vertx vertx;
    private final PingHandler pingHandler;

    public ServiceBuilder(AppToolKit appToolKit, PingHandler pingHandler) {
        this.vertx = appToolKit.getVertx();
        this.pingHandler = pingHandler;
    }

    public Future<HttpServer> buildAPIServer(int targetPort) {
        Promise<HttpServer> serverPromise = Promise.promise();
        vertx.executeBlocking((Promise<HttpServer> promise) -> {
            try {
                Router router = Router.router(vertx);

                router.get("/test").handler(pingHandler::getHelloWorld);
                router.get("/secondTest").handler(pingHandler::getHelloWorld2);

                HttpServer server = vertx.createHttpServer();
                server.requestHandler(router).listen(targetPort, serverStart -> {
                    if (serverStart.failed()) {
                        promise.fail(serverStart.cause());
                    } else {
                        if (serverStart.result().actualPort() != targetPort) {
                            promise.fail("Server started on wrong port!");
                        } else {
                            promise.complete(server);
                        }
                    }
                });


            } catch (Exception e) {
                promise.fail(e);
            }
        }, serverCreator -> {
            if (serverCreator.failed()) {
                serverPromise.fail(serverCreator.cause());
            } else {
                serverPromise.complete(serverCreator.result());
            }
        });
        return serverPromise.future();
    }


}
