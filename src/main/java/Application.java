import api.deployment.DeployService;
import io.micronaut.context.ApplicationContext;

public class Application {

    public static void main(String[] args) {
        try (ApplicationContext applicationContext = ApplicationContext.run()) {
            DeployService deployService = applicationContext.getBean(DeployService.class);
            deployService.deployServiceAPI();
        }
    }

}
